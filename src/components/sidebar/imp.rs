/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::CompositeTemplate;

#[derive(Default, CompositeTemplate)]
#[template(file = "./sidebar.ui")]
pub struct EchidnaSidebar {}

#[glib::object_subclass]
impl ObjectSubclass for EchidnaSidebar {
    const NAME: &'static str = "EchidnaSidebar";
    type Type = super::EchidnaSidebar;
    type ParentType = gtk::Box;

    fn class_init(klass: &mut Self::Class) {
        Self::bind_template(klass);
    }

    fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for EchidnaSidebar {}
impl WidgetImpl for EchidnaSidebar {}
impl BoxImpl for EchidnaSidebar {}
